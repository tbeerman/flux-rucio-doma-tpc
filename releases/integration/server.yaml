---
apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: domaservers
  namespace: rucio
  annotations:
    flux.weave.works/automated: "false"
spec:
  releaseName: domaservers
  chart:
    repository: https://rucio.github.io/helm-charts/
    name: rucio-server
    version: 0.8.2
  valuesFrom:
  - secretKeyRef:
      name: db-secret
      key: values.yaml
  values:
    replicaCount: 2
    authReplicaCount: 1
    traceReplicaCount: 0
    
    exposeErrorLogs:
      server: True
      authServer: True
    
    oidc:
      enable: True
    
    image:
      repository: rucio/rucio-server
      tag: release-1.22.4.dev1
      pullPolicy: Always
    
    httpd_config:
      mpm_mode: "event"
      enable_status: "True"

    monitoring:
      enabled: true

    resources:
      limits:
        cpu: "2"
        memory: "2Gi"
      requests:
        cpu: "1"
        memory: "1Gi"
    
    
    additionalSecrets:
      idpsecrets:
        secretName: idpsecrets
        mountPath: /opt/rucio/etc/idpsecrets.json
        subPath: idpsecrets.json
    
    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: nginx
        nginx.ingress.kubernetes.io/frontend-entry-points: http, https
        nginx.ingress.kubernetes.io/ssl-redirect: "false"
      hosts:
        - "rucio-doma.cern.ch"
        - "doma-tpc-rucio-l3h2wswpfzly-node-0.cern.ch"
      path: /
      tls:
        - secretName: rucio-doma-server.tls-secret
    
    traceServer:
      useSSL: false
    
    authServer:
      useSSL: true
      servicePort: 443
      targetPort: https
      portName: https
    
    authIngress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: nginx
        nginx.ingress.kubernetes.io/ssl-passthrough: "true"
        nginx.ingress.kubernetes.io/ssl-redirect: "true"
      hosts:
        - "rucio-doma-auth.cern.ch"
        - "doma-tpc-rucio-l3h2wswpfzly-node-1.cern.ch"
      path: /
    
    config:
      policy:
        permission: "atlas"
        schema: "atlas"
        lfn2pfn_algorithm_default: "identity"
    
      oidc:
        idpsecrets: "/opt/rucio/etc/idpsecrets.json"
        admin_issuer: "xdc"
    
      webui:
        auth_type: "oidc"
        auth_issuer: "xdc"
